<!--fichier public/index.php-->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Kevin Vincendeau - Zionlabs">
    <title>Gitlab auto deploy</title>
</head>
<body>
    <h1>Gitlab auto deploy</h1>
    <hr/>
    <h2>Server:</h2>
    <p>
        <?= $_SERVER['SERVER_NAME'] ?>
    </p>
    <h2>Last update:</h2>
    <p>
        <?= date('d F Y H:i:s', filemtime(__FILE__));  ?>
    </p>
</body>
</html>