
# Gitlab auto-deploy CI on FTP site

This Gitlab CI example works with two subdomains on the same server.
[More explanations in french](https://blog.zionlabs.fr/deploiement-continu-en-ftp-avec-gitlab-ci/)

#### Author
Kevin Vincendeau


## Prerequisites
Have a server with FTP and SSH accesses.

## On FTP server
Create a SSH ED25519 key.

## Environments in Gitlab
In gitlab, go to Operate > Environments and create these two environments:
- Preprod
- Prod

Only the name of environment is required.

## Environment variables in Gitlab

Go to Settings > CI/CD and click the Expand button on Variables line.
Create the variables:

| Key             | Value                                                                          | Type     | Protected | Masked | Expanded | Environment |
|-----------------|--------------------------------------------------------------------------------|----------|-----------|--------|----------|-------------|
| DEPLOY_HOST     | Ftp host URL                                                                   | variable |           | ✓      |          | All         |
| DEPLOY_PATH     | Internal path to public directory in preprod                                   | variable |           | ✓      |          | Preprod     |
| DEPLOY_PATH     | Internal path to public directory in prod                                      | variable |           | ✓      |          | Prod        |
| DEPLOY_USER     | Ftp username                                                                   | variable |           | ✓      |          | All         |
| SSH_PRIVATE_KEY | Content of private key file ed25519. Must contains a blank new line at the end | variable |           |        |          | All         |

## Use
 
On commiting on any branches except main, changes are deployed on the preprod path configured in DEPLOY_PATH.
On merge requests on main branches (or during commits to the main branch, even if it is bad practice), changes are 
deployed on the other path defined in DEPLOY_PATH for Prod environment.